#! /usr/bin/env python3

################################################################################
#
# Please consult the README for information on modifying these scripts and
# getting configuring LDAP to work with them.
#
################################################################################

from . import funcs

import os
import ldap3
from collections import namedtuple
import getpass
import argparse
import yaml
from pydoc import locate
from pathlib import Path

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


def findSettings(filename):
    path = None
    locations = (
        os.curdir,
        os.path.expanduser("~/.manageldap"),
        "/etc/manageldap",
        os.environ.get("MANAGELDAP_CONF", "."),
    )
    for loc in locations:
        loc_path = Path(os.path.join(loc, filename))
        if loc_path.is_file():
            path = os.path.join(loc, filename)
            break
    if path is None:
        raise Exception(
            "Config file " + str(filename) + " not found in " + str(locations)
        )
    return path


# YAML - Configuration
with open(findSettings("settings.yaml"), "r") as stream:
    config = yaml.load(stream, Loader=Loader)
with open(findSettings("people.yaml"), "r") as stream:
    people_config = yaml.load(stream, Loader=Loader)
with open(findSettings("groups.yaml"), "r") as stream:
    groups_config = yaml.load(stream, Loader=Loader)


Add = namedtuple("Add", "dn modlist")
Modify = namedtuple("Modify", "dn modlist")
Delete = namedtuple("Delete", "dn")
RDNMod = namedtuple("RDNMod", "dn new flag")
Transform = namedtuple("Transform", "dn attr fun")


class mlObject:
    def eval_function(self, arg):
        arg_dict = getattr(funcs, self.config[arg]["function"])(
            (self.args[arg]), self.config[arg]["provides"]
        )
        return arg_dict

    def eval_extra_function(self, arg):
        obj_list = getattr(funcs, self.config[arg]["extra_function"])(
            self, self.args[arg]
        )
        return obj_list

    def eval_auto_function(self, arg):
        arg_dict = getattr(funcs, self.config[arg]["auto_function"])(self)
        return arg_dict

    def eval_auto_extra_function(self, arg):
        obj_list = getattr(funcs, self.config[arg]["auto_extra_function"])(self)
        return obj_list

    def check_arg_valid(self, arg):
        if arg not in self.config:
            raise Exception("Unavailable argument '{}' provided".format(arg))

    def __add__(self, **kwargs):
        parser = argparse.ArgumentParser()
        parser = self.generate_parser(parser, "add")
        if len(kwargs):
            self.args = kwargs
        else:
            self.args = parser.parse_args().__dict__
        self.args = {arg: self.args[arg] for arg in self.args if self.args[arg]}
        attrs = {}
        extra_objects = []
        obj_list = []
        arg_dict = {}
        for arg in self.args:
            self.check_arg_valid(arg)
            if not self.args[arg]:
                continue
            elif "function" in self.config[arg]:
                arg_dict = self.eval_function(arg)
            elif "extra_function" in self.config[arg]:
                obj_list = self.eval_extra_function(arg)
            else:
                arg_dict = {i: self.args[arg] for i in self.config[arg]["provides"]}
            attrs.update(arg_dict)
            extra_objects.extend(obj_list)
        self.attrs = attrs
        for arg in self.config["attributes"]:
            if "auto" in self.config[arg] and arg not in self.args:
                arg_dict = {}
                obj_list = []
                # If already updated in previous statement skip this auto function
                if "provides" in self.config[arg] and all([c in attrs for c in  self.config[arg]["provides"]]):
                    continue
                if self.config[arg]["auto"]:
                    if "auto_extra_function" in self.config[arg]:
                        obj_list = self.eval_auto_extra_function(arg)
                    else:
                        arg_dict = self.eval_auto_function(arg)
                    attrs.update(arg_dict)
                    extra_objects.extend(obj_list)

        self.attrs = attrs
        dn = "{}={},{}".format(self.dnattr, self.attrs[self.dnattr], self.basedn)
        results = [Add(dn, {"objectClass": list(self.objectclasses), "attrs": attrs})]
        results.extend(extra_objects)
        return results

    def generate_parser(self, parser, action):
        for value in self.values:
            if "short" in self.config[value]:
                parser.add_argument(
                    "".join(("-", self.config[value]["short"])),
                    "".join(("--", value)),
                    type=locate(self.config[value]["type"]),
                    help=self.config[value]["help"],
                    default=self.config[value]["default"]
                    if "default" in self.config[value]
                    else "",
                    required=self.config[value]["required"]
                    if "required" in self.config[value]
                    else self.config[value]["{}required".format(action)]
                    if "{}required".format(action) in self.config[value]
                    else False,
                )
            else:
                parser.add_argument(
                    "".join(("--", value)),
                    type=locate(self.config[value]["type"]),
                    help=self.config[value]["help"],
                    default=self.config[value]["default"]
                    if "default" in self.config[value]
                    else "",
                    required=self.config[value]["required"]
                    if "required" in self.config[value]
                    else False,
                )
        return parser


class User(mlObject):
    def __init__(self):
        self.attributes = set()
        self.objectclasses = set(["top"])
        self.dnattr = config["People"]["dnattr"]
        self.basedn = "{},{}".format(
            config["Directory"]["usersrdn"], config["Directory"]["basedn"]
        )
        self.config = people_config
        self.settings = config
        self.values = self.config["attributes"]
        for value in self.values:
            if "provides" in self.config[value]:
                self.attributes |= set(self.config[value]["provides"])
            if "needs" in self.config[value]:
                self.objectclasses |= set(self.config[value]["needs"])
        self.main_parser = argparse.ArgumentParser(
            description="Manipulate user objects in LDAP Directory"
        )
        sub_parsers = self.main_parser.add_subparsers(help="possible actions")
        add_parser = sub_parsers.add_parser("add")
        add_parser = self.generate_parser(add_parser, "add")
        mod_parser = sub_parsers.add_parser("modify")
        mod_parser = self.generate_parser(mod_parser, "modify")
        del_parser = sub_parsers.add_parser("delete")
        del_parser = self.generate_parser(del_parser, "delete")

    def main(self):
        self.main_parser.parse_args()

    def add(self, **kwargs):
        results = self.__add__(**kwargs)
        handleLDIF(getConnection(), results)

    def mod(self, **kwargs):
        if len(kwargs):
            self.args = kwargs
        else:
            self.args = self.parser.parse_args().__dict__
        attrs = {}
        extra_objects = []
        obj_list = []
        arg_dict = {}
        for arg in self.args:
            self.check_arg_valid(arg)

            attrs.update(arg_dict)
            extra_objects.extend(obj_list)


class Group(mlObject):
    def __init__(self):
        self.attributes = set()
        self.objectclasses = set(["top"])
        self.dnattr = config["Group"]["dnattr"]
        self.basedn = "{},{}".format(
            config["Directory"]["groupsrdn"], config["Directory"]["basedn"]
        )
        self.config = groups_config
        self.values = self.config["attributes"]
        for value in self.values:
            if "provides" in self.config[value]:
                self.attributes |= set(self.config[value]["provides"])
            if "needs" in self.config[value]:
                self.objectclasses |= set(self.config[value]["needs"])
        self.parser = argparse.ArgumentParser(
            description="Manage groups in LDAP Directory"
        )
        for value in self.config["attributes"]:
            continue

    def add(self, **kwargs):
        results = self.__add__(**kwargs)
        return results

    def add_member(self, **kwargs):
        if len(kwargs):
            self.args = kwargs
        else:
            self.args = self.parser.parse_args().__dict__

        for arg in self.args:
            if arg not in self.config:
                raise Exception("Unavailable argument '{}' provided".format(arg))


class Connection:
    def __init__(self):
        print("test")

    def test(self):
        print("test")


def getNextId(database="", connection=""):
    """Return the next available id from passwd database"""
    if database == "group":
        entry = findEntry(
            connection,
            config["Directory"]["groupsrdn"] + "," + config["Directory"]["basedn"],
            "(objectClass=posixGroup)",
            ["cn", "gidNumber"],
        )
        maxid = config["POSIX"]["mingid"]
        for grp_entry in entry:
            gid = grp_entry["gidNumber"].value
            if ((config["POSIX"]["mingid"]) < gid + 1 < config["POSIX"]["maxgid"]) and (
                gid + 1 > maxid
            ):
                maxid = gid + 1
    else:
        entry = findEntry(
            connection,
            config["Directory"]["usersrdn"] + "," + config["Directory"]["basedn"],
            "(objectClass=posixAccount)",
            ["uid", "uidNumber"],
        )
        maxid = config["POSIX"]["minuid"]
        for user_entry in entry:
            uid = user_entry["uidNumber"].value
            if (config["POSIX"]["minuid"] < uid + 1 < config["POSIX"]["maxuid"]) and (
                uid + 1 > maxid
            ):
                maxid = uid + 1
    return maxid


def findEntry(connection, dn, filt, attr):
    with connection:
        connection.search(dn, filt, search_scope=ldap3.SUBTREE, attributes=attr)
        entry = connection.entries
    return entry


def getBindDn():
    # Return Admin-DN
    binddn = "uid={},{},{}".format(os.getlogin(), config['Directory']['adminsrdn'],config['Directory']['basedn'])
    return binddn


def getUsername(dn):
    """Get the uid/cn of the given DN."""
    return dn.split(",")[0].split("=")[1]


def gecosChange(connection, login, attr, value):
    """Get current gecos and return new gecos."""
    with connection:
        connection.search(
            search_base="ou=People,{}".format(config["Directory"]["basedn"]),
            search_filter="(&(gecos=*)(objectClass=posixAccount)\
                          (uid={}))".format(
                login
            ),
            attributes=["gecos"],
        )
        old_gecos = connection.response[0]["attributes"]["gecos"]
    split_gecos = old_gecos.split(",")
    attr_dict = {"name": 0, "room": 1, "phone": 2, "other": 3}
    split_gecos[attr_dict[attr]] = value
    new_gecos = ",".join(split_gecos)
    return new_gecos


def groupadd(group, gid=0, connection=""):
    gid = str(gid) if gid else getNextId(database="group", connection=connection)
    dn = "cn={},ou=Groups,{}".format(group, config["Directory"]["basedn"])
    results = []
    objectClass = ["posixGroup", "top"]
    attrs = {"cn": [group], "gidNumber": [gid]}
    results.append(Add(dn, {"objectClass": objectClass, "attrs": attrs}))
    return results


def groupmems(add="", delete="", group="", list=False, purge=False, connection=None):
    if not group:
        raise Exception("Expected a group name.")
    elif not (add or delete or list or purge):
        raise Exception(
            "Expected an action.\nPossible actions include:\n\t--add\n\t \
            --delete\n\t--list\n\t--purge"
        )
    dn = "cn={},ou=Groups,{}".format(group, config["Directory"]["basedn"])
    attrs = {}
    if add:
        attrs.update({"memberUid": [ldap3.MODIFY_ADD, add]})
    if delete:
        attrs.update({"memberUid": [ldap3.MODIFY_DELETE, delete]})
    if purge:
        # None indicates that all values for this attribute should be deleted
        attrs.update({"memberUid": [ldap3.MODIFY_DELETE, []]})
    if list:
        # This will need to fetch and format all of the users in this group.
        if connection is None:
            connection = getConnection()
        entry = findEntry(
            connection, dn, "(objectClass=posixGroup)", ("cn", "gidNumber", "memberUid")
        )
        print(entry)
        # raise Exception("groupmems cannot list users yet")
    return [Modify(dn, attrs)]


def groupmod(group, name="", gid=0):
    dn = "cn={},{},{}".format(
        group, config["Directory"]["groupsrdn"], config["Directory"]["basedn"]
    )
    results = []
    attrs = {}
    if name:
        results.append(RDNMod(dn, "cn={}".format(name), True))
    if gid:
        attrs.update({"gidNumber": [ldap3.MODIFY_REPLACE, gid]})
        results.insert(0, Modify(dn, attrs))
    return results


def usermod(
    user,
    groups=[],
    append=False,
    home="",
    name="",
    inactive=0,
    gid=0,
    login="",
    lock=False,
    move_home=False,
    shell="",
    uid=0,
    unlock=False,
    room="",
    phone="",
    other="",
    conn="",
):
    dn = "uid={},ou=People,{}".format(user, config["Directory"]["basedn"])
    results = []
    attrs = {}
    if groups:
        if append:
            for x in groups:
                results.extend(groupmems(add=user, group=x))
        else:
            raise Exception(
                "Removal of users from groups through usermod is not \
                yet supported. Please use groupmems.\nGroups not affected."
            )
    if home:
        if move_home:
            raise Exception(
                "Currently, usermod does not create a users home directory."
            )
        else:
            attrs.update({"homeDirectory": [ldap3.MODIFY_REPLACE, home]})
            print(
                "Note that without the --move-home option, \
                the users files will all remain in their old home directory."
            )
    if name:
        attrs.update({"cn": [ldap3.MODIFY_REPLACE, name]})
        name_split = name.split(" ")
        sn = name_split.pop(-1)
        gn = " ".join(name_split)
        attrs.update({"givenName": [ldap3.MODIFY_REPLACE, gn]})
        attrs.update({"sn": [ldap3.MODIFY_REPLACE, sn]})
        attrs.update(
            {"gecos": [ldap3.MODIFY_REPLACE, gecosChange(conn, user, "name", name)]}
        )
    if room:
        attrs.update({"roomNumber": [ldap3.MODIFY_REPLACE, room]})
        if "gecos" in attrs.keys():
            ldif = attrs["gecos"]
            gecos = ldif[1].split(",")
            gecos[1] = room
            ldif[1] = ",".join(gecos)
            attrs.update({"gecos": ldif})
        else:
            attrs.update(
                {"gecos": [ldap3.MODIFY_REPLACE, gecosChange(conn, user, "room", room)]}
            )
    if phone:
        attrs.update({"homePhone": [ldap3.MOD_REPLACE, phone]})
        if "gecos" in attrs.keys():
            ldif = attrs["gecos"]
            gecos = ldif[1].split(",")
            gecos[2] = phone
            ldif[1] = ",".join(gecos)
            attrs.update({"gecos": ldif})
        else:
            attrs.update(
                {
                    "gecos": [
                        ldap3.MODIFY_REPLACE,
                        gecosChange(conn, user, "phone", phone),
                    ]
                }
            )

    if gid:
        gid = str(gid)
        attrs.update({"gidNumber": [ldap3.MODIFY_REPLACE, gid]})

    if login:
        # This will require changing the actual record, which will require
        # slightly different changes.
        results.append(
            RDNMod(
                "uid={},ou=People,{}".format(user, config["Directory"]["basedn"]),
                "uid={}".format(login),
                True,
            )
        )
    """
    if lock:
        # This will require some string manipulation of the crypted password. An
        # exclamation point (!) must be added in front of the crypted password.
        attrs.append((ldap3.MOD_REPLACE, 'loginShell', "/usr/sbin/nologin"))
        results.append(
            Transform(
                dn,
                "userPassword",
                lambda pw: pw if pw.startswith("!") else "!" +
                pw))
    """
    if shell:
        attrs.update({"loginShell": [ldap3.MODIFY_REPLACE, shell]})
    if uid:
        uid = str(uid)
        attrs.update({"uidNumber": [ldap3.MODIFY_REPLACE, uid]})
    """
    if unlock:
        # This will require some string manipulation of the crypted
        # password. An exclamation point (!) must be remove from the front
        # of the crypted password.
        attrs.append((ldap3.MOD_REPLACE, 'loginShell', "/bin/bash"))
        results.append(
            Transform(
                dn,
                "userPassword",
                lambda pw: pw[
                    1:] if pw.startswith("!") else pw))
    """
    moddeduser = Modify(dn, attrs)
    results.insert(0, moddeduser)
    return results


def handleLDIF(connection, ldif_s):
    """Handle processing a given LDIF using the provided connection."""
    if isinstance(ldif_s, list):
        for entry in ldif_s:
            handleLDIF(connection, entry)
        return 0

    else:
        action = type(ldif_s)
        entry = ldif_s
    with connection:
        try:
            if action == Add:
                connection.add(
                    entry.dn, entry.modlist["objectClass"], entry.modlist["attrs"]
                )
            elif action == Delete:
                connection.delete(entry.dn, entry.modlist)
            elif action == Modify:
                connection.modify(entry.dn, entry.modlist)
            elif action == RDNMod:
                connection.modify_dn(entry.dn, entry.new)
            elif action == Transform:
                a = connection.search(
                    entry.dn, ldap3.SCOPE_SUBTREE, "(objectClass=person)", [entry.attr]
                )[0][1][entry.attr]
                modlist = map(
                    lambda y: (ldap3.MODIFY_REPLACE, entry.attr, entry.fun(y)), a
                )
                connection.modify(entry.dn, modlist)
            else:
                print(action)
                raise Exception("Unknown action type.")
            if connection.result['result'] != 0:
                print(entry)
                print(connection.result)
        except Exception as e:
            print("Unexpected error:", e.args)


"""   except ldap3.TYPE_OR_VALUE_EXISTS:
        print(
            "The value that you are trying to apply to attribute in '{}' \
            is already set and exists.".format(ldif_s.dn))
        print(ldif_s)
    except ldap3.ALREADY_EXISTS:
        print("This value already exists in the directory:")
        print(ldif_s)
    except ldap3.INSUFFICIENT_ACCESS:
        print("You do not have sufficient access to perform:", ldif_s)
    except ldap3.NO_SUCH_OBJECT:
        print("No appropriate object was found in the directory.\
            This may be caused by a previous failure to add an object\
            that you are now trying to modify. The associated change is:",
              ldif_s)

"""


def getConnection(
    dn=getBindDn(),
    server=config["Connection"]["server"],
    passwd="",
    tls=bool(config["Connection"]["tls"]),
    _port=int(config["Connection"]["port"]),
):
    # FIXME implement SASL
    connection_server = ldap3.Server(
        host=server, port=_port, get_info="ALL", use_ssl=tls
    )
    if not passwd:
        passwd = getpass.getpass("Bind password for {}:".format(dn))
    connection = ldap3.Connection(connection_server, user=dn, password=passwd)
    connection.bind()
    if connection.result["result"]:
        raise Exception(
            "\nResult: "
            + "{}\n".format(connection.result["result"])
            + "Description: "
            + "{}".format(connection.result["description"])
        )
    else:
        connection.unbind()
        return connection


def update(actions, connection=""):
    if not connection:
        connection = getConnection(
            dn=getBindDn(), server=config["Connection"]["server"]
        )
    # Process all of our actions.
    for action in actions:
        handleLDIF(connection, action)


def isauto(nested_dict):
    if "auto" in nested_dict:
        if nested_dict["auto"] == "yes":
            yield nested_dict["id"]
        for k in nested_dict:
            if isinstance(nested_dict[k], list):
                for i in nested_dict[k]:
                    for j in isauto(i):
                        yield j
