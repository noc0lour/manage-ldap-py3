# /usr/bin/env python3
import re
import manageldap as ml
from ldap3 import utils
import getpass


def splitname(name, output):
    result = dict()
    result.update({output[0]: re.split(" ", name)[0] if name else ""})
    result.update({output[1]: re.split(" ", name)[-1] if name else ""})
    result.update({output[2]: name})
    result.update({output[3]: ','.join([re.split(' ',name)[-1],re.split(' ',name)[0]]) if name else ''})
    return result


def splitaddress(address, output):
    result = dict()
    result.update({output[0]: re.split(",", address)[0] if address else ""})
    result.update(
        {output[1]: re.split(" ", re.split(",", address)[-1])[-2] if address else ""}
    )
    result.update(
        {output[2]: re.split(" ", re.split(",", address)[-1])[-1] if address else ""}
    )
    return result


def addusergroup(self):
    g = ml.Group()
    group = g.__add__(name=self.args["login"], gid=self.attrs["uidnumber"], member=self.args['login'])
    return group

def addmember(memberlist, output):
    result = dict()
    memberlist = memberlist.replace(" ", "").split(',')
    for member in memberlist:
        result.update({output[0]: member})
    return result


def changegecos(gecos, output):
    return {"gecos": gecos}


def generatehome(self):
    result = dict()
    result.update(
        {self.config["home"]["provides"][0]: "/home/{}".format(self.attrs["uid"])}
    )
    return result


def generatemail(self):
    result = dict()
    result.update(
        {
            self.config["mail"]["provides"][0]: "{}@{}".format(
                self.attrs["uid"], self.settings['Directory']['maildomain']
            )
        }
    )
    result.update(
        {
            self.config["mail"]["provides"][1]: "{}.{}@{}".format(
                self.attrs["givenName"], self.attrs["sn"], self.settings['Directory']['maildomain']
            )
        }
    )
    return result

def generatemailhostquota(self):
    result = dict()
    result.update({self.config['mailhost']['provides'][0]: 'localhost'})
    result.update({self.config['mailhost']['provides'][1]: 2048})
    return result


def generatepassword(password, output):
    result = dict()
    hashed_pass = utils.hashed.hashed(utils.hashed.HASHED_SALTED_SHA512, password)
    result.update({output[0]: hashed_pass})
    return result


def askpassword(self):
    result = dict()
    pass1 = 0
    pass2 = 1
    while pass1 != pass2:
        pass1 = getpass.getpass("Password for new user:")
        pass2 = getpass.getpass("Confirm password:")
    hashed_pass = utils.hashed.hashed(utils.hashed.HASHED_SALTED_SHA512, pass1)
    pass1 = 0
    pass2 = 0
    result.update({self.config["password"]["provides"][0]: hashed_pass})
    return result


def getnextid(self):
    result = dict()
    uid = ml.getNextId(database="user", connection=ml.getConnection())
    result.update({"uidnumber": uid})
    result.update({"gidnumber": uid})
    return result
